module.exports = {
    isUserNameValid: function (username) {
        if (username.length < 3 || username.length > 15) {
            return false;
        }
        if (username.toLowerCase() !== username) {
            return false;
        }
        return true;
    },
    isAgeValid: function (age) {
        var Age = parseInt(age);
        if (Age < 18 || Age > 100) {
            return false;
        }
        if (isNaN(age)) {
            return false;
        }
        return true;
    },
    isPasswordValid: function (pass) {
        if (pass.length < 8) {
            return false;
        }
        if (!pass.match(/[A-Z]{1,}/)) {
            return false;
        }
        if (!pass.match(/[0-9]{3,}/)) {
            return false;
        }
        if (!pass.match(/[ !@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]/)) {
            return false;
        }
        return true;
    },
    isDateValid: function (day, month, year) {
        if (day < 1 || day > 31) {
            return false;
        }
        if (month > 12){
            return false;
        }
        if (year < 1970 || year > 2020){
            return false;
        }
        if ((month = 1 && day < 31)) {
            return true;
        }
        if ((month = 2 && day < 30)) {
            return true;
        }
        if ((month = 3 && day < 31)) {
            return true;
        }
        if ((month = 4 && day < 30)) {
            return true;
        }
        if ((month = 5 && day < 31)) {
            return true;
        }
        if ((month = 6 && day < 30)) {
            return true;
        }
        if ((month = 7 && day < 31)) {
            return true;
        }
        if ((month = 8 && day < 31)) {
            return true;
        }
        if ((month = 9 && day < 30)) {
            return true;
        }
        if ((month = 10 && day < 31)) {
            return true;
        }
        if ((month = 11 && day < 30)) {
            return true;
        }
        if ((month = 12 && day < 31)) {
            return true;
        }
        return true;
    }


}
